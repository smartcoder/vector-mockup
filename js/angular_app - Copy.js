var vector = angular.module('vector', ['ui.bootstrap']);
vector.controller('AbtCtrl', function ($scope,$http) {
	// ---------------------------- ABT SCREEN RELATED CODE ----------------//
	$scope.abts = [
	  {
	    "abt_sk": 1,
	    "abt_short_nm": "Movie Pattern",
	    "abt_desc": "User movie watching pattern",
	  },
	  {
	    "abt_sk": 2,
	    "abt_short_nm": "Program Category TV",
	    "abt_desc": "Includes TV program category pattern",
	  },
	  {
	    "abt_sk": 3,
	    "abt_short_nm": "Movie-1",
	    "abt_desc": "How often user watches TV in last three months",
	  },
	]
	$scope.edit_var = {};
	$scope.variables=[];
	$scope.show_fields = function($event,abt){
		$event.preventDefault();
		$scope.current_abt = abt;
		$scope.variables = [
		  {
		    "variable_sk": 1,
		    "variable_column_nm": "C_CNT_DTV_VDU_L3M",
		    "variable_short_nm": "Count ViewedDuration   of Last 3 Months",
		    "variable_desc": "Count ViewedDuration   of Last 3 Months",
		  },
		  {
		    "variable_sk": 2,
		    "variable_column_nm": "C_CNT_MOV_DTV_VDU_L3M",
		    "variable_short_nm": "Count ViewedDuration of Movies  of Last 3 Months",
		    "variable_desc": "Count ViewedDuration of Movies  of Last 3 Months",
		  },
		  {
		    "variable_sk": 3,
		    "variable_column_nm": "C_CNT_R_DTV_VDU_L3M",
		    "variable_short_nm": "Count ViewedDuration of R  of Last 3 Months",
		    "variable_desc": "Count ViewedDuration of R  of Last 3 Months",
		  },
		  {
		    "variable_sk": 4,
		    "variable_column_nm": "C_CNT_TV7_DTV_VDU_L3M",
		    "variable_short_nm": "Count ViewedDuration of TVY7  of Last 3 Months",
		    "variable_desc": "Count ViewedDuration of TVY7  of Last 3 Months",
		  },
		  {
		    "variable_sk": 5,
		    "variable_column_nm": "C_CNT_P13_DTV_VDU_L3M",
		    "variable_short_nm": "Count ViewedDuration of PG-13  of Last 3 Months",
		    "variable_desc": "Count ViewedDuration of PG-13  of Last 3 Months",
		  },
		]
	}
	$scope.show_save_abt=function($event){
    $event.preventDefault();
    $("#modal-abts-add").modal("show");
  }  
  $scope.show_edit_abt=function($event,abt){
    $event.preventDefault();
    $scope.new_abt={};
    angular.copy(angular.fromJson(abt), $scope.new_abt);
    $("#modal-abts-add").modal("show");
  }
  $scope.show_build_abt=function($event){
    $event.preventDefault();
    $("#modal-build-abt").modal("show");
  }
  $scope.show_edit_variable=function($event,variable){
    $event.preventDefault();
    angular.copy(angular.fromJson(variable), $scope.edit_var);
    $('#modal-variable-edit').modal('show');
  }

  // ---------------------------- VARIABLE SCREEN RELATED CODE ----------------//
  $scope.var_types = [
	  {
	    "label": "Behavioural Variables",
	    "code": "BEH",
	  },
	  {
	    "label": "Time based Variables",
	    "code": "RNT",
	  },
	  {
	    "label": "Supplementary Variables",
	    "code": "SPM",
	  },
	  {
	    "label": "Arithmetic Derived Variables",
	    "code": "DER",
	  },
	  {
	    "label": "Logical Derived Variables",
	    "code": "DER",
	  }
	]
	$scope.current_var_type=$scope.var_types[0];
	
	$scope.select_var_type=function($event, var_type){
    $event.preventDefault();
    $scope.current_var_type=var_type;
  }
	$scope.show_var_attributes=function(data_source, level){
    $("#modal-var-attributes").modal("show");
  }
  $scope.toggle_node_options=function($event,node){
    $event.preventDefault();
    if(node.show_all){
      node.show_all = false;
    }else{
      node.show_all = true;
    }
  }
  $scope.toggle_tree_node=function($event,level, element){
    console.log("toggling", element);
    $event.preventDefault();
    switch(element){
      case "var_attributes_toggle":
        level.var_attributes_toggle=!level.var_attributes_toggle;
        break;
      case "sel_criteria_toggle":
        level.sel_criteria_toggle=!level.sel_criteria_toggle;
        break;
      case "agg_list_toggle":
        level.agg_list_toggle=!level.agg_list_toggle;
        break;
      case "time_period_list_toggle":
        level.time_period_list_toggle=!level.time_period_list_toggle;
        break;
      case "display_columns_toggle":
        level.display_columns_toggle=!level.display_columns_toggle;
        break;
    } 
  }
  $scope.levels = [
	  {
	    "level_sk": 1,
	    "level_short_nm": "Access Card",
	    "level_assoc_sk": 1,
	    "level_assoc_short_nm": "Access Card",
	    "level_cd": "C",
	    "variables": [
	      null,
	      [
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": "",
	          "physical_name": "C_SUM_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration   of Last 1 Month",
	          "description": "Total ViewedDuration   of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            8
	          ],
	          "physical_name": "C_SUM_TRA_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of TRAILER  of Last 1 Month",
	          "description": "Total ViewedDuration of TRAILER  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_8",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            4
	          ],
	          "physical_name": "C_SUM_NTV_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of NON-LINEAR TV  of Last 1 Month",
	          "description": "Total ViewedDuration of NON-LINEAR TV  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_4",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            60
	          ],
	          "physical_name": "C_SUM_RCD_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of RecordingEvent  of Last 1 Month",
	          "description": "Total ViewedDuration of RecordingEvent  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_60",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            7
	          ],
	          "physical_name": "C_SUM_Rem_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of RemoteBooking  of Last 1 Month",
	          "description": "Total ViewedDuration of RemoteBooking  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_7",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            3
	          ],
	          "physical_name": "C_SUM_MOV_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of MOVIE  of Last 1 Month",
	          "description": "Total ViewedDuration of MOVIE  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_3",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            59
	          ],
	          "physical_name": "C_SUM_PLB_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of PlaybackEvent  of Last 1 Month",
	          "description": "Total ViewedDuration of PlaybackEvent  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_59",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            6
	          ],
	          "physical_name": "C_SUM_Pur_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of Purchase  of Last 1 Month",
	          "description": "Total ViewedDuration of Purchase  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_6",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            2
	          ],
	          "physical_name": "C_SUM_EPI_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of EPISODE  of Last 1 Month",
	          "description": "Total ViewedDuration of EPISODE  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_2",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            5
	          ],
	          "physical_name": "C_SUM_OTH_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of OTHERS  of Last 1 Month",
	          "description": "Total ViewedDuration of OTHERS  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_5",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "BEH",
	          "attribute_value_sks": [
	            1
	          ],
	          "physical_name": "C_SUM_Dow_DTV_VDU_L1M",
	          "display_name": "Total ViewedDuration of Download  of Last 1 Month",
	          "description": "Total ViewedDuration of Download  of Last 1 Month",
	          "measures": "ViewedDuration",
	          "measure_sk": 4,
	          "column_data_type_sk": 1,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "aggregation": "Total",
	          "aggregation_type_sk": 1,
	          "variable_definition": "BEH_1_1_1_4_1_1",
	          "variable_type_sk": 1,
	          "source_table_sk": 1
	        }
	      ],
	      [
	        {
	          "var_type": "RNT",
	          "attribute_value_sks": "",
	          "physical_name": "C_LST_DTV_VDU_L1M_ETM",
	          "display_name": "Last ViewedDuration for the Last 1 Month",
	          "description": "Last ViewedDuration for the Last 1 Month",
	          "order_by_date": "ETM",
	          "order_by_date_source_column_sk": 2,
	          "display_column": 1,
	          "column_data_type_sk": 1,
	          "select_source_column_sk": 4,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "variable_definition": "RNT_1_1_4_1_2",
	          "variable_type_sk": 2,
	          "source_table_sk": 1
	        },
	        {
	          "var_type": "RNT",
	          "attribute_value_sks": [
	            8
	          ],
	          "physical_name": "C_LST_TRA_DTV_VDU_L1M_ETM",
	          "display_name": "Last TRAILER ViewedDuration for the Last 1 Month",
	          "description": "Last TRAILER ViewedDuration for the Last 1 Month",
	          "order_by_date": "ETM",
	          "order_by_date_source_column_sk": 2,
	          "display_column": 1,
	          "column_data_type_sk": 1,
	          "select_source_column_sk": 4,
	          "time_period": "Last 1 Month",
	          "time_period_sk": 1,
	          "variable_definition": "RNT_1_1_4_1_2_8",
	          "variable_type_sk": 2,
	          "source_table_sk": 1
	        }
	      ],
	      [],
	      []
	    ],
	    "class": "active",
	    "data_sources": [
	      {
	        "level_sk": 1,
	        "source_table_sk": 1,
	        "source_table_cd": "DTV",
	        "source_table_nm": "directv_new",
	        "source_table_short_nm": "directv",
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "abt_sk": 28,
	        "table_usage_cd": "BEH",
	        "purpose_sk": 1004
	      }
	    ],
		  "var_attributes": [
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 4,
	        "source_column_cd": "VDU",
	        "column_data_type_sk": 1,
	        "source_column_short_nm": "ViewedDuration"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 10,
	        "source_column_cd": "PDU",
	        "column_data_type_sk": 1,
	        "source_column_short_nm": "Duration"
	      }
	    ],
	    "sel_criteria": [
	      {
	        "source_column_short_nm": "EventType",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "RecordingEvent",
	            "attribute_value_cd": "RCD",
	            "attribute_value_sk": 60,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "Download",
	            "attribute_value_cd": "Dow",
	            "attribute_value_sk": 1,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "Purchase",
	            "attribute_value_cd": "Pur",
	            "attribute_value_sk": 6,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "PlaybackEvent",
	            "attribute_value_cd": "PLB",
	            "attribute_value_sk": 59,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "OTHERS",
	            "attribute_value_cd": "OTH",
	            "attribute_value_sk": 5,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "NON-LINEAR TV",
	            "attribute_value_cd": "NTV",
	            "attribute_value_sk": 4,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "MOVIE",
	            "attribute_value_cd": "MOV",
	            "attribute_value_sk": 3,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "TRAILER",
	            "attribute_value_cd": "TRA",
	            "attribute_value_sk": 8,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "EPISODE",
	            "attribute_value_cd": "EPI",
	            "attribute_value_sk": 2,
	            "selected": false
	          },
	          {
	            "attribute_value_short_nm": "RemoteBooking",
	            "attribute_value_cd": "Rem",
	            "attribute_value_sk": 7,
	            "selected": false
	          }
	        ],
	        "source_column_cd": "ETP",
	        "selected": false
	      },
	      {
	        "source_column_short_nm": "Language",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "it",
	            "attribute_value_cd": "it",
	            "attribute_value_sk": 54
	          },
	          {
	            "attribute_value_short_nm": "fr",
	            "attribute_value_cd": "fr",
	            "attribute_value_sk": 53
	          },
	          {
	            "attribute_value_short_nm": "es",
	            "attribute_value_cd": "es",
	            "attribute_value_sk": 52
	          },
	          {
	            "attribute_value_short_nm": "en",
	            "attribute_value_cd": "en",
	            "attribute_value_sk": 51
	          },
	          {
	            "attribute_value_short_nm": "de",
	            "attribute_value_cd": "de",
	            "attribute_value_sk": 50
	          },
	          {
	            "attribute_value_short_nm": "pt",
	            "attribute_value_cd": "pt",
	            "attribute_value_sk": 55
	          }
	        ],
	        "source_column_cd": "LNG"
	      },
	      {
	        "source_column_short_nm": "ProgramCategory",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "Adult",
	            "attribute_value_cd": "ADL",
	            "attribute_value_sk": 28
	          },
	          {
	            "attribute_value_short_nm": "TV",
	            "attribute_value_cd": "TV",
	            "attribute_value_sk": 31
	          },
	          {
	            "attribute_value_short_nm": "Sports",
	            "attribute_value_cd": "SPT",
	            "attribute_value_sk": 30
	          },
	          {
	            "attribute_value_short_nm": "Movies",
	            "attribute_value_cd": "MOV",
	            "attribute_value_sk": 29
	          }
	        ],
	        "source_column_cd": "PCG"
	      },
	      {
	        "source_column_short_nm": "Rating",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "NR (Not Rated)",
	            "attribute_value_cd": "NR",
	            "attribute_value_sk": 33
	          },
	          {
	            "attribute_value_short_nm": "TVG",
	            "attribute_value_cd": "TVG",
	            "attribute_value_sk": 38
	          },
	          {
	            "attribute_value_short_nm": "G",
	            "attribute_value_cd": "G",
	            "attribute_value_sk": 32
	          },
	          {
	            "attribute_value_short_nm": "TV14",
	            "attribute_value_cd": "TV1",
	            "attribute_value_sk": 37
	          },
	          {
	            "attribute_value_short_nm": "TVY7",
	            "attribute_value_cd": "TV7",
	            "attribute_value_sk": 42
	          },
	          {
	            "attribute_value_short_nm": "R",
	            "attribute_value_cd": "R",
	            "attribute_value_sk": 36
	          },
	          {
	            "attribute_value_short_nm": "TVY",
	            "attribute_value_cd": "TVY",
	            "attribute_value_sk": 41
	          },
	          {
	            "attribute_value_short_nm": "PG-13",
	            "attribute_value_cd": "P13",
	            "attribute_value_sk": 35
	          },
	          {
	            "attribute_value_short_nm": "TVPG",
	            "attribute_value_cd": "TVP",
	            "attribute_value_sk": 40
	          },
	          {
	            "attribute_value_short_nm": "PG",
	            "attribute_value_cd": "PG",
	            "attribute_value_sk": 34
	          },
	          {
	            "attribute_value_short_nm": "TVMA",
	            "attribute_value_cd": "TVM",
	            "attribute_value_sk": 39
	          }
	        ],
	        "source_column_cd": "RTG"
	      },
	      {
	        "source_column_short_nm": "UserDeviceTypeName",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "IPAD",
	            "attribute_value_cd": "IPA",
	            "attribute_value_sk": 12
	          },
	          {
	            "attribute_value_short_nm": "TABLET",
	            "attribute_value_cd": "TAB",
	            "attribute_value_sk": 17
	          },
	          {
	            "attribute_value_short_nm": "DOTCOM",
	            "attribute_value_cd": "DOT",
	            "attribute_value_sk": 11
	          },
	          {
	            "attribute_value_short_nm": "SetTopBox",
	            "attribute_value_cd": "STB",
	            "attribute_value_sk": 16
	          },
	          {
	            "attribute_value_short_nm": "ANDROID TABLET",
	            "attribute_value_cd": "ATA",
	            "attribute_value_sk": 10
	          },
	          {
	            "attribute_value_short_nm": "SMARTPHONE",
	            "attribute_value_cd": "SPH",
	            "attribute_value_sk": 15
	          },
	          {
	            "attribute_value_short_nm": "ANDROID PHONE",
	            "attribute_value_cd": "APH",
	            "attribute_value_sk": 9
	          },
	          {
	            "attribute_value_short_nm": "OTHERS",
	            "attribute_value_cd": "OTH",
	            "attribute_value_sk": 14
	          },
	          {
	            "attribute_value_short_nm": "IPHONE",
	            "attribute_value_cd": "IPH",
	            "attribute_value_sk": 13
	          }
	        ],
	        "source_column_cd": "UDT"
	      },
	      {
	        "source_column_short_nm": "ViewedDayOfTime",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "Daytime",
	            "attribute_value_cd": "DTM",
	            "attribute_value_sk": 22
	          },
	          {
	            "attribute_value_short_nm": "Prime Fringe",
	            "attribute_value_cd": "PFR",
	            "attribute_value_sk": 27
	          },
	          {
	            "attribute_value_short_nm": "Late Night",
	            "attribute_value_cd": "LNG",
	            "attribute_value_sk": 26
	          },
	          {
	            "attribute_value_short_nm": "Late Evening",
	            "attribute_value_cd": "LVN",
	            "attribute_value_sk": 25
	          },
	          {
	            "attribute_value_short_nm": "Early Morning",
	            "attribute_value_cd": "EMO",
	            "attribute_value_sk": 24
	          },
	          {
	            "attribute_value_short_nm": "Early Fringe",
	            "attribute_value_cd": "EFR",
	            "attribute_value_sk": 23
	          }
	        ],
	        "source_column_cd": "VDT"
	      },
	      {
	        "source_column_short_nm": "ViewedDay",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "Tuesday",
	            "attribute_value_cd": "TUE",
	            "attribute_value_sk": 21
	          },
	          {
	            "attribute_value_short_nm": "Saturday",
	            "attribute_value_cd": "SAT",
	            "attribute_value_sk": 58
	          },
	          {
	            "attribute_value_short_nm": "Thursday",
	            "attribute_value_cd": "THU",
	            "attribute_value_sk": 20
	          },
	          {
	            "attribute_value_short_nm": "Friday",
	            "attribute_value_cd": "FRI",
	            "attribute_value_sk": 57
	          },
	          {
	            "attribute_value_short_nm": "Sunday",
	            "attribute_value_cd": "SUN",
	            "attribute_value_sk": 19
	          },
	          {
	            "attribute_value_short_nm": "Wednesday",
	            "attribute_value_cd": "WED",
	            "attribute_value_sk": 56
	          },
	          {
	            "attribute_value_short_nm": "Monday",
	            "attribute_value_cd": "MON",
	            "attribute_value_sk": 18
	          }
	        ],
	        "source_column_cd": "VDY"
	      },
	      {
	        "source_column_short_nm": "ViewRegion",
	        "attributes": [
	          {
	            "attribute_value_short_nm": "Atlantic",
	            "attribute_value_cd": "ATC",
	            "attribute_value_sk": 44
	          },
	          {
	            "attribute_value_short_nm": "Pacific",
	            "attribute_value_cd": "PCF",
	            "attribute_value_sk": 49
	          },
	          {
	            "attribute_value_short_nm": "Alaska",
	            "attribute_value_cd": "ASK",
	            "attribute_value_sk": 43
	          },
	          {
	            "attribute_value_short_nm": "Mountain",
	            "attribute_value_cd": "MTN",
	            "attribute_value_sk": 48
	          },
	          {
	            "attribute_value_short_nm": "Hawaii",
	            "attribute_value_cd": "HWL",
	            "attribute_value_sk": 47
	          },
	          {
	            "attribute_value_short_nm": "Eastern",
	            "attribute_value_cd": "EST",
	            "attribute_value_sk": 46
	          },
	          {
	            "attribute_value_short_nm": "Central",
	            "attribute_value_cd": "CNT",
	            "attribute_value_sk": 45
	          }
	        ],
	        "source_column_cd": "VRG"
	      }
	    ],
	    "agg_list": [
	      {
	        "aggregation_type_sk": 1,
	        "aggregation_type_cd": "SUM",
	        "aggregation_type_short_nm": "Total",
	        "aggregation_type_desc": "Total"
	      },
	      {
	        "aggregation_type_sk": 2,
	        "aggregation_type_cd": "AVG",
	        "aggregation_type_short_nm": "Average by time",
	        "aggregation_type_desc": "Average by time"
	      },
	      {
	        "aggregation_type_sk": 3,
	        "aggregation_type_cd": "MAX",
	        "aggregation_type_short_nm": "Maximum",
	        "aggregation_type_desc": "Maximum"
	      },
	      {
	        "aggregation_type_sk": 4,
	        "aggregation_type_cd": "MIN",
	        "aggregation_type_short_nm": "Minimum",
	        "aggregation_type_desc": "Minimum"
	      },
	      {
	        "aggregation_type_sk": 5,
	        "aggregation_type_cd": "CNT",
	        "aggregation_type_short_nm": "Count",
	        "aggregation_type_desc": "Count"
	      },
	      {
	        "aggregation_type_sk": 6,
	        "aggregation_type_cd": "AVC",
	        "aggregation_type_short_nm": "Average by record",
	        "aggregation_type_desc": "Average by record"
	      }
	    ],
	    "time_period_list": [
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 1,
	        "time_period_cd": "L1M",
	        "time_period_short_nm": "Last 1 Month"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 2,
	        "time_period_cd": "L3M",
	        "time_period_short_nm": "Last 3 Months"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 3,
	        "time_period_cd": "13_24M",
	        "time_period_short_nm": "Last 13 to 24 Months"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 6,
	        "time_period_cd": "1_12M",
	        "time_period_short_nm": "Last 12 Months"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 8,
	        "time_period_cd": "3_6M",
	        "time_period_short_nm": "Last 3 to 6 Months"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 10,
	        "time_period_cd": "L6M",
	        "time_period_short_nm": "Last 6 Months"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 11,
	        "time_period_cd": "3MB",
	        "time_period_short_nm": "3 Months back"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 12,
	        "time_period_cd": "6MB",
	        "time_period_short_nm": "6 Months back"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 13,
	        "time_period_cd": "12MB",
	        "time_period_short_nm": "12 Months back"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 14,
	        "time_period_cd": "24MB",
	        "time_period_short_nm": "24 Months back"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 15,
	        "time_period_cd": "DFTCAP",
	        "time_period_short_nm": "Account Default Capture Period"
	      },
	      {
	        "frequency_sk": 1,
	        "frequency_cd": "MONTH",
	        "time_period_sk": 16,
	        "time_period_cd": "2MB",
	        "time_period_short_nm": "2 Months back"
	      }
	    ],
		  "current_dates": [
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 2,
	        "source_column_cd": "ETM",
	        "source_column_short_nm": "EventTime",
	        "table_usage_cd": "RNT"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 2,
	        "source_column_cd": "ETM",
	        "source_column_short_nm": "EventTime",
	        "table_usage_cd": "RNT"
	      }
	    ],
	    "current_date": {
	      "source_table_sk": 1,
	      "source_column_sk": 2,
	      "source_column_cd": "ETM",
	      "source_column_short_nm": "EventTime",
	      "table_usage_cd": "RNT"
	    },
	    "display_columns": [
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 4,
	        "source_column_cd": "VDU",
	        "column_data_type_sk": 1,
	        "source_column_short_nm": "ViewedDuration"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 10,
	        "source_column_cd": "PDU",
	        "column_data_type_sk": 1,
	        "source_column_short_nm": "Duration"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 3,
	        "source_column_cd": "ETP",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "EventType"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 5,
	        "source_column_cd": "UDT",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "UserDeviceTypeName"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 6,
	        "source_column_cd": "VDY",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "ViewedDay"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 7,
	        "source_column_cd": "VDT",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "ViewedDayOfTime"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 8,
	        "source_column_cd": "PCG",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "ProgramCategory"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 9,
	        "source_column_cd": "RTG",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "Rating"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 11,
	        "source_column_cd": "VRG",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "ViewRegion"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 12,
	        "source_column_cd": "CHT",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "ChannelType"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 13,
	        "source_column_cd": "STR",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "StateRegions"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 14,
	        "source_column_cd": "LNG",
	        "column_data_type_sk": 2,
	        "source_column_short_nm": "Language"
	      },
	      {
	        "source_table_sk": 1,
	        "source_column_sk": 2,
	        "source_column_cd": "ETM",
	        "column_data_type_sk": 4,
	        "source_column_short_nm": "EventTime"
	      }
	    ]
	  }
	]
	$scope.current_level_obj = {};
	$scope.current_level_obj.current_level = $scope.levels[0];
})