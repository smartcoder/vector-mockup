google.load('visualization', '1', {packages:['orgchart']});
var vector = angular.module('vector', ['ui.bootstrap']);
vector.directive('orgchart', function() {
  return {
    restrict: 'E',
    link: function($scope, $elm) {
      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.OrgChart($elm[0]);
      chart.draw($scope.orgChartData, {
        allowHtml: true
      });
    }
  }
});
vector.controller('AbtCtrl', function ($scope,$http,behService,rntService,spmService,derService) {
	// ---------------------------- ABT SCREEN RELATED CODE ----------------//
	$scope.dateOptions = {
    startingDay: 1
  };  
  $scope.date_open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
	$scope.abts = [
	  {
	    "abt_sk": 1,
	    "abt_short_nm": "Sample ABT #1",
	    "abt_desc": "Description of Sample ABT #1",
	  },
	  {
	    "abt_sk": 2,
	    "abt_short_nm": "Sample ABT #2",
	    "abt_desc": "Description of Sample ABT #2",
	  },
	  {
	    "abt_sk": 3,
	    "abt_short_nm": "Sample ABT #3",
	    "abt_desc": "Description of Sample ABT #3",
	  },
	]
	$scope.edit_var = {};
	$scope.variables=[];
	$scope.show_fields = function($event,abt){
		if($event){
			$event.preventDefault();
		}
		if(abt){
			$scope.current_abt = abt;
		}
		$scope.variables = [
		  {
		    "variable_sk": 1,
		    "variable_column_nm": "field_sample_abt_1",
		    "variable_short_nm": "Variable #1",
		    "variable_desc": "Description of Variable #1",
		  },
		  {
		    "variable_sk": 2,
		    "variable_column_nm": "field_sample_abt_2",
		    "variable_short_nm": "Variable #2",
		    "variable_desc": "Description of Variable #2",
		  },
		  {
		    "variable_sk": 3,
		    "variable_column_nm": "field_sample_abt_3",
		    "variable_short_nm": "Variable #3",
		    "variable_desc": "Description of Variable #3",
		  },
		  {
		    "variable_sk": 4,
		    "variable_column_nm": "field_sample_abt_4",
		    "variable_short_nm": "Variable #4",
		    "variable_desc": "Description of Variable #4",
		  },
		  {
		    "variable_sk": 5,
		    "variable_column_nm": "field_sample_abt_5",
		    "variable_short_nm": "Variable #5",
		    "variable_desc": "Description of Variable #5",
		  },
		]
	}
	$scope.show_save_abt=function($event){
    $event.preventDefault();
    $("#modal-abts-add").modal("show");
  }  
  $scope.show_edit_abt=function($event,abt){
    $event.preventDefault();
    $scope.new_abt={};
    angular.copy(angular.fromJson(abt), $scope.new_abt);
    $("#modal-abts-add").modal("show");
  }
  $scope.show_build_abt=function($event){
    $event.preventDefault();
    $("#modal-build-abt").modal("show");
  }
  $scope.show_edit_variable=function($event,variable){
    $event.preventDefault();
    angular.copy(angular.fromJson(variable), $scope.edit_var);
    $('#modal-variable-edit').modal('show');
  }

  // ---------------------------- VARIABLE SCREEN RELATED CODE ----------------//
  $scope.var_types = [
	  {
	    "label": "Behavioural Variables",
	    "code": "BEH",
	  },
	  {
	    "label": "Time based Variables",
	    "code": "RNT",
	  },
	  {
	    "label": "Supplementary Variables",
	    "code": "SPM",
	  },
	  {
	    "label": "Arithmetic Derived Variables",
	    "code": "DER",
	  },
	  {
	    "label": "Logical Derived Variables",
	    "code": "DER",
	  }
	]
	$scope.current_var_type=$scope.var_types[0];
	
	$scope.select_var_type=function($event, var_type){
    $event.preventDefault();
    $scope.current_var_type=var_type;
    if(var_type.code == 'BEH'){
	    $scope.levels = behService.get_json();
		}
		if(var_type.code == 'RNT'){
			$scope.levels = rntService.get_json();	
		}
		if(var_type.code == 'SPM'){
			$scope.levels = spmService.get_json();
		}
		if(var_type.code == 'DER'){
			$scope.levels = derService.get_json();
		}
	}
	$scope.show_var_attributes=function(data_source, level,var_code){
		if(var_code == 'BEH'){
			$scope.levels = behService.get_json();
		}
		if(var_code == 'RNT'){
			$scope.levels = rntService.get_json();	
		}
		if(var_code == 'SPM'){
			$scope.levels = spmService.get_json();
		}
    $("#modal-var-attributes").modal("show");
  }
  $scope.toggle_node_options=function($event,node){
    $event.preventDefault();
    if(node.show_all){
      node.show_all = false;
    }else{
      node.show_all = true;
    }
  }
  $scope.toggle_tree_node=function($event,level, element){
    console.log("toggling", element);
    $event.preventDefault();
    switch(element){
      case "var_attributes_toggle":
        level.var_attributes_toggle=!level.var_attributes_toggle;
        break;
      case "sel_criteria_toggle":
        level.sel_criteria_toggle=!level.sel_criteria_toggle;
        break;
      case "agg_list_toggle":
        level.agg_list_toggle=!level.agg_list_toggle;
        break;
      case "time_period_list_toggle":
        level.time_period_list_toggle=!level.time_period_list_toggle;
        break;
      case "display_columns_toggle":
        level.display_columns_toggle=!level.display_columns_toggle;
        break;
    } 
  }
  $scope.levels = [
	  {
	    "level_sk": 1,
	    "level_short_nm": "Access Card",
	    "level_assoc_sk": 1,
	    "level_assoc_short_nm": "Access Card",
	    "variables": [
	      null,
	      [
	        {
	          "var_type": "BEH",
	          "physical_name": "Variable#1",
	          "display_name": "Total Viewed Duration of Last 1 Month",
	          "description": "Total ViewedDuration of Last 1 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 1 Month",
	        },
	        {
	          "var_type": "BEH",
	          "physical_name": "Variable#2",
	          "display_name": "Total Viewed Duration of Last 3 Month",
	          "description": "Total ViewedDuration of Last 3 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 3 Month",
	        },
	        {
	          "var_type": "BEH",
	          "physical_name": "Variable#3",
	          "display_name": "Total Viewed Duration of Last 6 Month",
	          "description": "Total ViewedDuration of Last 6 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 6 Month",
	        },
	      ],
	      [
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#4",
	          "display_name": "Total Viewed Duration of Last 9 Month",
	          "description": "Total ViewedDuration of Last 9 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 9 Month",
	          "order_by_date": "ETM",
	        },
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#5",
	          "display_name": "Total Viewed Duration of Last 12 Month",
	          "description": "Total ViewedDuration of Last 12 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 12 Month",
	          "order_by_date": "ETM",
	        },
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#6",
	          "display_name": "Total Viewed Duration of Last 2 Year",
	          "description": "Total ViewedDuration of Last 2 Year",
	          "measures": "ViewedDuration",
	          "time_period": "Last 2 Year",
	          "order_by_date": "ETM",
	        },
	      ],
	      [
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#44",
	          "display_name": "Total Viewed Duration of Last 9 Month",
	          "description": "Total ViewedDuration of Last 9 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 9 Month",
	          "order_by_date": "ETM",
	        },
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#55",
	          "display_name": "Total Viewed Duration of Last 12 Month",
	          "description": "Total ViewedDuration of Last 12 Month",
	          "measures": "ViewedDuration",
	          "time_period": "Last 12 Month",
	          "order_by_date": "ETM",
	        },
	        {
	          "var_type": "RNT",
	          "physical_name": "Variable#66",
	          "display_name": "Total Viewed Duration of Last 2 Year",
	          "description": "Total ViewedDuration of Last 2 Year",
	          "measures": "ViewedDuration",
	          "time_period": "Last 2 Year",
	          "order_by_date": "ETM",
	        },
	      ],
	      [
	        {
	          "var_type": "DER",
	          "var_sub_type": "ATH",
	          "physical_name": "Variable#7",
	          "variable_definition": "Variable#7",
	          "description": "Total ViewedDuration of Last 2 Year",
	          "display_name": "Variable#7",
	          "expression": "( Variable#1 + Variable#2 )",
	        },
	        {
	          "var_type": "DER",
	          "var_sub_type": "ATH",
	          "physical_name": "Variable#8",
	          "variable_definition": "Variable#8",
	          "description": "Total ViewedDuration of Last 2 Year",
	          "display_name": "Variable#8",
	          "expression": "( Variable#3 X Variable#4 )",
	        },
	        {
	          "var_type": "DER",
	          "var_sub_type": "ATH",
	          "physical_name": "Variable#9",
	          "variable_definition": "Variable#9",
	          "description": "Total ViewedDuration of Last 2 Year",
	          "display_name": "Variable#9",
	          "expression": "( Variable#5 / Variable#6 )",
	        },
	      ],
	      [
	        {
	          "var_type": "DER",
	          "var_sub_type": "LGC",
	          "physical_name": "Variable#10",
	          "display_name": "Variable#10",
	          "description": "Variable#10",
	          "expression": " CASE  WHEN  THEN 'id'  ELSE 'type'  END ",
	        },
	        {
	          "var_type": "DER",
	          "var_sub_type": "LGC",
	          "physical_name": "Variable#11",
	          "display_name": "Variable#11",
	          "description": "Variable#11",
	          "expression": " CASE  WHEN  THEN 'amount'  ELSE 'tax'  END ",
	        }
	      ]
	    ],
	    "class": "active",
	    "data_sources": [
	      {
	        "level_sk": 1,
	        "source_table_sk": 1,
	        "source_table_cd": "ETV",
	        "source_table_nm": "example_tv",
	        "source_table_short_nm": "ExampleTV",
	        "abt_sk":1,
	      }
	    ]
	  }
	]
	$scope.current_level_obj = {};
	$scope.current_level_obj.current_level = $scope.levels[0];

	$scope.show_der_var_attributes=function(level, var_label){
		$scope.levels = derService.get_json();
    switch(var_label){
      case "Arithmetic Derived Variables":
        show_arithmetic_var_attributes();
        break;
		  case "Logical Derived Variables":
        show_logical_var_attributes();
        break;
    }
  }
  $scope.numerical_vars = [];
	show_arithmetic_var_attributes=function(){
		$scope.show_fields(null,null);
		$scope.numerical_vars = $scope.variables;
		$scope.arith_var={
      physical_name:"",
      display_name:"",
      description:"",
      expression:"",
      curr_numerical_var:{}
	  };
    $("#modal-var-arith").modal("show");
  }

  show_logical_var_attributes=function(){
    $scope.logical_var={};
    $("#modal-var-logical-top").modal("show");
  }

  $scope.arith_var_validation={
    last_expn:{}, //used for validation
    bracket_open:0,
    error_msg:"",
  }
  $scope.add_operator_to_expn=function(operator,op_type){
    console.log("Checking operator"+operator+" of type "+op_type);
    console.log("Arith var validation is ", $scope.arith_var_validation);
    $scope.arith_var_validation.error_msg = "";
    switch(op_type){
      case "operator":
        if(!$scope.arith_var_validation.last_expn.type){
          $scope.arith_var_validation.error_msg = "You cannot start with an operator.";
          return false;
        }
        if($scope.arith_var_validation.last_expn.type=='operator'){
          $scope.arith_var_validation.error_msg = "You cannot put an operator after another operator";
          return false;
        }
        break;
      case "function":
        if($scope.arith_var_validation.last_expn.type=='variable' || $scope.arith_var_validation.last_expn.type=='number'){
          $scope.arith_var_validation.error_msg = "Please add an operator first";
          return false;
        }
        $scope.arith_var_validation.bracket_open++;
        break;
      case "number":
        if(!$scope.arith_var_validation.last_expn.type){
          $scope.arith_var_validation.error_msg = "You cannot start with an Number.";
          return false;
        }
        if($scope.arith_var_validation.last_expn.type=='variable'){
          $scope.arith_var_validation.error_msg = "Add either an operator (+ - / *)";
          return false;
        }
        break;
      case "bracket":
        if(operator=='(' && (($scope.arith_var_validation.last_expn.type && $scope.arith_var_validation.last_expn.type != 'operator') || $scope.arith_var.expression != "")){
          $scope.arith_var_validation.error_msg = "Please add an operator first ";
          return false;
        }
        if(operator==")" && $scope.arith_var_validation.bracket_open<=0){
          $scope.arith_var_validation.error_msg = "Add opening brackets first.";
          return false;
        }
        if(operator=='('){
          $scope.arith_var_validation.bracket_open++;
        }else{
          $scope.arith_var_validation.bracket_open--;
        }
        break;
      default:
    }
    $scope.arith_var.expression+=operator;
    $scope.arith_var_validation.last_expn={
      type:op_type,
      value:operator
    };
  }

  $scope.add_var_to_expn=function(){
    console.log("Going to add Numerical var to expn", $scope.arith_var.curr_numerical_var);
    $scope.arith_var_validation.error_msg = "";
		if(!$scope.arith_var_validation.last_expn || $scope.arith_var_validation.last_expn.type=='variable'){
		  $scope.arith_var_validation.error_msg = "Please add an operator or function first";
		  return false;
		}
		if($scope.arith_var_validation.last_expn.type=='number'){
		  $scope.arith_var_validation.error_msg = "Please add an operator first";
		  return false;
		}
		var var_name = $scope.arith_var.curr_numerical_var.variable_column_nm;
		if(var_name){
		  $scope.arith_var_validation.last_expn={
		      type:"variable",
		      value:var_name
		  };
		  $scope.arith_var.expression+=" " +var_name+" ";
		  if(!$scope.arith_var.original_vars){
		      $scope.arith_var.original_vars=[];
		  }
		  $scope.arith_var.original_vars.push($scope.arith_var.curr_numerical_var);
		}else{
		  $scope.arith_var_validation.error_msg = "Please select any operator first";
		}
  }

  $scope.clear_arith_var_expn=function(){
    if(confirm("Are you sure you want to start again?\nThis action cannot be reverted.")){
      $scope.arith_var.expression="";
      $scope.arith_var_validation={
        last_expn:{}, //used for validation
        bracket_open:0,
        error_msg:"",
      }
    }
  }

  $scope.add_category_inputs=function(){
    console.log("Adding new categories, ",$scope.logical_var.category_count);
    $scope.logical_var.categories=[];
    for(i=0;i<$scope.logical_var.category_count;i++){
      $scope.logical_var.categories.push({});
    }
  }

  $scope.create_logical_var_categories=function(){
    var err_msg=""
    angular.forEach($scope.logical_var.categories, function(category, key){
      if(!category.name){
        err_msg="The category names cannot be blank";
        return;
      }
    });

    if(err_msg){
      alert(err_msg);
      return false;
    }
    $("#modal-var-logical-top").modal("hide");
    var parent_node_id=createUUID();
    $scope.logical_var.nodes=[
			{
				id:parent_node_id,
				parent_id:"",
				parent_name:"",
				condition_name:"Categories",
				is_category_name:false,
				is_top_node:true
			}
    ];
    angular.forEach($scope.logical_var.categories, function(category, key){
      $scope.logical_var.nodes.push(
        {
          id:createUUID(),
          parent_id:parent_node_id,
          parent_name:"Categories",
          is_category_name:true,
          is_top_node:false,
          condition_is_default:category.is_default,
          condition_name:category.name,
        }
      )
    });
    $("#modal-var-logical").modal("show");
	  // Now configure the node for google charts
	  $scope.show_condition_tree();
  }

  $scope.show_condition_tree=function(){
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('string', 'Manager');
    data.addColumn('string', 'ToolTip');
    var arr_nodes=[];
    angular.forEach($scope.logical_var.nodes, function(node, key){
      var cls_hide="";
      if(!node.parent_id){
        cls_hide="hide";
      }else{
        cls_hide="";
      }

      var cls_default_hide='';
      if(node.condition_is_default){
        cls_default_hide="hide";
      }
      var new_node=[
        {
          v:node.id, 
          f:node.condition_name+'<br><button class="btn btn-default btn-xs btn-condition-add '+cls_hide+' '+cls_default_hide+ '" data-parentid="'+node.id+'" data-parentname="'+node.condition_name+'"><img src="images/add.gif"></button> <button class="btn btn-default btn-xs btn-condition-delete '+cls_hide+'" data-id="'+node.id+'" data-nodename="'+node.condition_name+'"><img src="images/delete.gif"</button>'
        }, 
        node.parent_id, 
        "Click + button to add more conditions"
      ]
      arr_nodes.push(new_node)
    });
    data.addRows(arr_nodes);
    var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
    chart.draw(data, {allowHtml:true});
  }
  function createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
	}
})