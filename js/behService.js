vector.service('behService', function(){
  this.get_json= function(){
    return [
      {
        "level_sk": 1,
        "level_short_nm": "Access Card",
        "level_assoc_sk": 1,
        "level_assoc_short_nm": "Access Card",
        "variables": [
          null,
          [
            {
              "var_type": "BEH",
              "physical_name": "Variable#1",
              "display_name": "Total Viewed Duration of Last 1 Month",
              "description": "Total ViewedDuration of Last 1 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 1 Month",
            },
            {
              "var_type": "BEH",
              "physical_name": "Variable#2",
              "display_name": "Total Viewed Duration of Last 3 Month",
              "description": "Total ViewedDuration of Last 3 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 3 Month",
            },
            {
              "var_type": "BEH",
              "physical_name": "Variable#3",
              "display_name": "Total Viewed Duration of Last 6 Month",
              "description": "Total ViewedDuration of Last 6 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 6 Month",
            },
          ],
          [
            {
              "var_type": "RNT",
              "physical_name": "Variable#4",
              "display_name": "Total Viewed Duration of Last 9 Month",
              "description": "Total ViewedDuration of Last 9 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 9 Month",
              "order_by_date": "ETM",
            },
            {
              "var_type": "RNT",
              "physical_name": "Variable#5",
              "display_name": "Total Viewed Duration of Last 12 Month",
              "description": "Total ViewedDuration of Last 12 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 12 Month",
              "order_by_date": "ETM",
            },
            {
              "var_type": "RNT",
              "physical_name": "Variable#6",
              "display_name": "Total Viewed Duration of Last 2 Year",
              "description": "Total ViewedDuration of Last 2 Year",
              "measures": "ViewedDuration",
              "time_period": "Last 2 Year",
              "order_by_date": "ETM",
            },
          ],
          [
            {
              "var_type": "RNT",
              "physical_name": "Variable#44",
              "display_name": "Total Viewed Duration of Last 9 Month",
              "description": "Total ViewedDuration of Last 9 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 9 Month",
              "order_by_date": "ETM",
            },
            {
              "var_type": "RNT",
              "physical_name": "Variable#55",
              "display_name": "Total Viewed Duration of Last 12 Month",
              "description": "Total ViewedDuration of Last 12 Month",
              "measures": "ViewedDuration",
              "time_period": "Last 12 Month",
              "order_by_date": "ETM",
            },
            {
              "var_type": "RNT",
              "physical_name": "Variable#66",
              "display_name": "Total Viewed Duration of Last 2 Year",
              "description": "Total ViewedDuration of Last 2 Year",
              "measures": "ViewedDuration",
              "time_period": "Last 2 Year",
              "order_by_date": "ETM",
            },
          ],
          [
            {
              "var_type": "DER",
              "var_sub_type": "ATH",
              "physical_name": "Variable#7",
              "variable_definition": "Variable#7",
              "description": "Total ViewedDuration of Last 2 Year",
              "display_name": "Variable#7",
              "expression": "( Variable#1 + Variable#2 )",
            },
            {
              "var_type": "DER",
              "var_sub_type": "ATH",
              "physical_name": "Variable#8",
              "variable_definition": "Variable#8",
              "description": "Total ViewedDuration of Last 2 Year",
              "display_name": "Variable#8",
              "expression": "( Variable#3 X Variable#4 )",
            },
            {
              "var_type": "DER",
              "var_sub_type": "ATH",
              "physical_name": "Variable#9",
              "variable_definition": "Variable#9",
              "description": "Total ViewedDuration of Last 2 Year",
              "display_name": "Variable#9",
              "expression": "( Variable#5 / Variable#6 )",
            },
          ],
          [
            {
              "var_type": "DER",
              "var_sub_type": "LGC",
              "physical_name": "Variable#10",
              "display_name": "Variable#10",
              "description": "Variable#10",
              "expression": " CASE  WHEN  THEN 'id'  ELSE 'type'  END ",
            },
            {
              "var_type": "DER",
              "var_sub_type": "LGC",
              "physical_name": "Variable#11",
              "display_name": "Variable#11",
              "description": "Variable#11",
              "expression": " CASE  WHEN  THEN 'amount'  ELSE 'tax'  END ",
            }
          ]
        ],
        "class": "active",
        "data_sources": [
          {
            "level_sk": 1,
            "source_table_sk": 1,
            "source_table_cd": "ETV",
            "source_table_nm": "example_tv",
            "source_table_short_nm": "ExampleTV",
            "abt_sk":1,
          }
        ],
        "var_attributes": [
          {
            "source_table_sk": 1,
            "source_column_sk": 4,
            "source_column_cd": "VDU",
            "source_column_short_nm": "ViewedDuration"
          },
          {
            "source_table_sk": 1,
            "source_column_sk": 10,
            "source_column_cd": "PDU",
            "source_column_short_nm": "Duration"
          }
        ],
        "sel_criteria": [
          {
            "source_column_short_nm": "Language",
            "source_column_cd": "LNG",
            "attributes": [
              {
                "attribute_value_short_nm": "English",
                "attribute_value_cd": "en",
                "attribute_value_sk": 54
              },
              {
                "attribute_value_short_nm": "Franch",
                "attribute_value_cd": "fr",
                "attribute_value_sk": 53
              },
              {
                "attribute_value_short_nm": "Arabic",
                "attribute_value_cd": "ar",
                "attribute_value_sk": 52
              },
              {
                "attribute_value_short_nm": "Dutch",
                "attribute_value_cd": "du",
                "attribute_value_sk": 51
              },
            ],
          },
          {
            "source_column_short_nm": "ProgramCategory",
            "source_column_cd": "PCG",
            "attributes": [
              {
                "attribute_value_short_nm": "Adult",
                "attribute_value_cd": "ADL",
                "attribute_value_sk": 28
              },
              {
                "attribute_value_short_nm": "Sports",
                "attribute_value_cd": "SPT",
                "attribute_value_sk": 30
              },
              {
                "attribute_value_short_nm": "Movies",
                "attribute_value_cd": "MOV",
                "attribute_value_sk": 29
              }
            ],
          },
          {
            "source_column_short_nm": "Rating",
            "source_column_cd": "RTG",
            "attributes": [
              {
                "attribute_value_short_nm": "NR (Not Rated)",
                "attribute_value_cd": "NR",
                "attribute_value_sk": 33
              },
              {
                "attribute_value_short_nm": "TVG",
                "attribute_value_cd": "TVG",
                "attribute_value_sk": 38
              },
              {
                "attribute_value_short_nm": "G",
                "attribute_value_cd": "G",
                "attribute_value_sk": 32
              },
            ],
          },
          {
            "source_column_short_nm": "UserDeviceTypeName",
            "source_column_cd": "UDT",
            "attributes": [
              {
                "attribute_value_short_nm": "IPAD",
                "attribute_value_cd": "IPA",
                "attribute_value_sk": 12
              },
              {
                "attribute_value_short_nm": "TABLET",
                "attribute_value_cd": "TAB",
                "attribute_value_sk": 17
              },
              {
                "attribute_value_short_nm": "DOTCOM",
                "attribute_value_cd": "DOT",
                "attribute_value_sk": 11
              },
              {
                "attribute_value_short_nm": "SetTopBox",
                "attribute_value_cd": "STB",
                "attribute_value_sk": 16
              },
              {
                "attribute_value_short_nm": "SMARTPHONE",
                "attribute_value_cd": "SPH",
                "attribute_value_sk": 15
              },
            ],
          },
        ],
        "agg_list": [
          {
            "aggregation_type_sk": 1,
            "aggregation_type_cd": "SUM",
            "aggregation_type_short_nm": "Total",
            "aggregation_type_desc": "Total"
          },
          {
            "aggregation_type_sk": 2,
            "aggregation_type_cd": "AVG",
            "aggregation_type_short_nm": "Average",
            "aggregation_type_desc": "Average"
          },
          {
            "aggregation_type_sk": 3,
            "aggregation_type_cd": "MAX",
            "aggregation_type_short_nm": "Maximum",
            "aggregation_type_desc": "Maximum"
          },
          {
            "aggregation_type_sk": 4,
            "aggregation_type_cd": "MIN",
            "aggregation_type_short_nm": "Minimum",
            "aggregation_type_desc": "Minimum"
          },
          {
            "aggregation_type_sk": 5,
            "aggregation_type_cd": "CNT",
            "aggregation_type_short_nm": "Count",
            "aggregation_type_desc": "Count"
          },
        ],
        "time_period_list": [
          {
            "time_period_sk": 1,
            "time_period_cd": "L1M",
            "time_period_short_nm": "Last 1 Month"
          },
          {
            "time_period_sk": 2,
            "time_period_cd": "L3M",
            "time_period_short_nm": "Last 3 Months"
          },
          {
            "time_period_sk": 3,
            "time_period_cd": "L6M",
            "time_period_short_nm": "Last 6 Months"
          },
          {
            "time_period_sk": 4,
            "time_period_cd": "L9M",
            "time_period_short_nm": "Last 9 Months"
          },
          {
            "time_period_sk": 5,
            "time_period_cd": "L12M",
            "time_period_short_nm": "Last 12 Months"
          },
        ]
      }
    ]
  }
});